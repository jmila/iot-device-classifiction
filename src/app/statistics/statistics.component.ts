import { Component, OnDestroy, OnInit } from '@angular/core';
import { PaperClassification } from '../models/paperclassification.model';
import { ClassificationService } from '../services/classification.service';
import { DialogModule } from 'primeng/dialog';
import * as FileSaver from 'file-saver';
import { MenuItem } from 'primeng/api/menuitem';
import { HttpClient } from '@angular/common/http';
import { TreeNode } from 'primeng/api';
import { FileUtilsService } from '../services/file-utils.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.scss']
})
export class StatisticsComponent implements OnInit {

	papersClassifications!: PaperClassification[];

  	displayBib: boolean = false;
  	displayPaperOfId: string = "";

 	categoriesSub !: Subscription;

  	selectedBibFormat: string = 'SPLNCS03';

  	visibleColumns: any[] = [];
	
	stats: number[]=[];
	
	columnNames: any[]=[];
	columnNamesIndex: any[]=[];
	
	statsOrder: number[]= [];
	
	colors: any[]=[];

	colorsMap=new Map();
	
	labelsMap= new Map();
	/**
	* Chart attributes
	*/
	basicData: any;
	basicOptions: any;
	multiAxisData: any;
	chartOptions: any;
	multiAxisOptions: any;
	stackedData: any;
	stackedOptions: any;
	horizontalOptions: any;

  	constructor(public classificationService: ClassificationService, private http: HttpClient) { }

  	ngOnInit() {
//		let statsG = new Array(35);
    	this.classificationService.getPapersClassification()?.then(data =>{
			this.papersClassifications = data
			this.stats = new Array(35);
	  		for(let i =0; i<35; i++){
	  			this.stats[i]=0
	  		}
			const stats = this.stats
	  		data.forEach(function(value){
				if(value.data){
					value.data.forEach(function(val, index){
						if(val){
							stats[index] += 1/57*100;
						}
					});
				}
	  		});
	
			this.labelsMap.set("DeepSkyBlue","Input: The classified devices");
			this.labelsMap.set("IndianRed","Acquisition method");
			this.labelsMap.set("OrangeRed","Trafic type");
			this.labelsMap.set("Wheat","Capture point");
			this.labelsMap.set("MediumSeaGreen","Packet level features");
			this.labelsMap.set("Violet","Stream definition");
			this.labelsMap.set("DarkOrchid","Stream features");
			this.labelsMap.set("Silver","Dimensionality reduction");
			this.labelsMap.set("SlateGray","Deep learning based feature extraction");
			this.labelsMap.set("SandyBrown","Number of classifiers");
			this.labelsMap.set("PaleTurquoise","Availability of labeled data");
			this.labelsMap.set("SpringGreen","Depth of classifiers");
			this.labelsMap.set("Thistle","Evaluation metrics");
			this.labelsMap.set("HotPink","Output: the classification granularity");
			
			
			//------------Input: The classified devices------------------//
			this.colorsMap.set("Only IoT devices","DeepSkyBlue");
			this.colorsMap.set("Both IoT and non IoT devices","DeepSkyBlue");
			//-----------------------------------------------------------//
			//------------Acquisition method-----------------------------//
			this.colorsMap.set("Use of public dataset","IndianRed");
			this.colorsMap.set("Share publicly","IndianRed");
			this.colorsMap.set("Keep private","IndianRed");
			//-----------------------------------------------------------//
			//------------Traffic Type-----------------------------------//
			this.colorsMap.set("Setup","OrangeRed");
			this.colorsMap.set("Idle","OrangeRed");
			this.colorsMap.set("Interaction","OrangeRed");
			//-----------------------------------------------------------//
			//------------Capture point----------------------------------//
			this.colorsMap.set("At the gateway","Wheat");
			this.colorsMap.set("After the gateway","Wheat");
			//-----------------------------------------------------------//
			//------------Packet level features--------------------------//
			this.colorsMap.set("Header","MediumSeaGreen");
			this.colorsMap.set("Payload","MediumSeaGreen");
			//-----------------------------------------------------------//
			//------------Stream definition------------------------------//
			this.colorsMap.set("Finite sequence of N packets","Violet");
			this.colorsMap.set("Packets exchanged within a time period","Violet");
			this.colorsMap.set("A flow","Violet");
			//-----------------------------------------------------------//
			//------------Stream features------------------------------//
			this.colorsMap.set("Volume features","DarkOrchid");
			this.colorsMap.set("Protocol features","DarkOrchid");
			this.colorsMap.set("Time related features","DarkOrchid");
			this.colorsMap.set("Periodicity features","DarkOrchid");
			//-----------------------------------------------------------//
			//------------Dimensionality reduction-----------------------//
			this.colorsMap.set("Dimensionality reduction","Silver");
			//-----------------------------------------------------------//
			//------------Deep learning based feature exteraction--------//
			this.colorsMap.set("Deep learning based feature extraction","SlateGray");
			//-----------------------------------------------------------//
			//------------Number of classifiers--------------------------//
			this.colorsMap.set("One-class classifier","SandyBrown");
			this.colorsMap.set("Multi-class classifier","SandyBrown");
			//-----------------------------------------------------------//
			//------------Availability of labeled data-------------------//
			this.colorsMap.set("Supervised learning","PaleTurquoise");
			this.colorsMap.set("Unsupervised learning","PaleTurquoise");
			//-----------------------------------------------------------//
			//------------Depth of classifiers-------------------//
			this.colorsMap.set("Deep learning","SpringGreen");
			this.colorsMap.set("Shallow learning","SpringGreen");
			//-----------------------------------------------------------//
			//------------Evaluation metrics-------------------//
			this.colorsMap.set("Evaluation metrics","Thistle");
			//-----------------------------------------------------------//
			//------------Output: the classification granularity-------------------//
			this.colorsMap.set("Device category","HotPink");
			this.colorsMap.set("Device type","HotPink");
			this.colorsMap.set("Device instance","HotPink");
			//-----------------------------------------------------------//
//			console.log(this.colorsMap);
			this.stats = stats;
			this.statsOrder = [];
			this.colors=[];
			for(var i=0;i<this.columnNames.length;i++){
				this.statsOrder.push(this.stats[this.columnNamesIndex[i]]);
				this.colors.push(this.colorsMap.get(this.columnNames[i]));
			}
			console.log(this.colors);
			this.changeChart();
			
		});
	    this.categoriesSub = this.classificationService.categoriesSubject.subscribe((categories) => {
	    	this.visibleColumns = this.extractVisibleColumns(categories);
		  	this.columnNames = this.classificationService.getVisibleLeafCategories();
			this.columnNamesIndex = this.classificationService.getVisibleLeafCategoriesDataIndex();
//			console.log(this.columnNamesIndex);
//		  	console.log(this.columnNames);
	      	if (this.visibleColumns[0]) {
	        	this.visibleColumns[0].unshift({ label: "Paper", childrenAmount: 0 });
	      	}
//			console.log(this.visibleColumns);
			this.statsOrder = [];
			this.colors=[]
//			console.log(this.colorsMap);
			for(var i=0;i<this.columnNames.length;i++){
				//Recup le plus grand parent
//				console.log(this.classificationService.getCategory(this.columnNames[i]))			
				
				this.statsOrder.push(this.stats[this.columnNamesIndex[i]]);
				this.colors.push(this.colorsMap.get(this.columnNames[i]));
				
			}
//			console.log(this.colors);
			this.changeChart();
	    });
	    this.classificationService.emitCategories();


		//=======================CHART CONFIGURAITON=======================//
		this.multiAxisOptions = {
            plugins: {
                legend: {labels: {color: '#495057'},display:false},
                tooltips: {mode: 'index',intersect: true}
            },
            scales: {
                x: {
                    ticks: {color: '#495057'},
                    grid: {color: '#ebedef'},
					stacked:false,
                },
                y: {
                    type: 'linear',
                    display: true,
                    position: 'left',
                    ticks: {min: 0,max: 100,color: '#495057'},
                    grid: {color: '#ebedef'},
					stacked:false
                },
            },
//			legend: { display:false }
        };
  	}
	

 	ngOnDestroy(): void {
    	this.categoriesSub.unsubscribe();
  	}

  	getLevelsAmount() {
    	return this.visibleColumns.length;
  	}
	changeChart(){
		/*var lastValue = "";
		var datasetTable = new Array();
		var dataset = {label:'',backgroundColor:'',yAxisID:'y',data:new Array()};
		var i=0;
		var statsTable = new Array();
		this.colorsMap.forEach((value, key) => {
			console.log("Map : value = "+value+", key = "+key);
			if(lastValue == value){
				//Ajouter dans le même tableau
				console.log(i+" : "+this.stats[this.columnNamesIndex[i]]);
				statsTable.push(this.stats[this.columnNamesIndex[i]])
			}else{
				//push l'ancien dans le dataSetTable
				if(i !=0){
					datasetTable.push(dataset); 
				}
				//créer un nouveau tableau
				statsTable=[]; 
				statsTable.push(this.stats[this.columnNamesIndex[i]])
				dataset = {label:this.labelsMap.get(value),backgroundColor:value,yAxisID:'y',data:statsTable};
			}
			
			lastValue = value;
			console.log(lastValue);
			i++;
		});
		console.log(datasetTable);*/
			
		
		this.multiAxisData = {
            labels:this.columnNames,
			options:{label:{display:false}},
            datasets: [{
//                label: ["Root"],
                backgroundColor: this.colors,
                yAxisID: 'y',
                data: this.statsOrder
        	}]
    	};
//		this.multiAxisData = {
//            labels:this.columnNames,
//            datasets: datasetTable
//    	};
	}
  	tempColumns!: any[];
	
	/**
   	* childrenAmount: colspan for the column
   	* @param categories 
   	* @returns 
   	*/
  	extractVisibleColumns(categories: any): any[] {
    	this.tempColumns = [];

	    this.extractVisibleColumnsIn(categories, 0);
	    // console.log("extractVisiColum: ");
	    // this.tempColumns.forEach((v) => {
	    //   v.forEach((c: { label: string; childrenAmount: number; }) => {
	    //     console.log("extractVisiColum val:" + c.label + ": " + c.childrenAmount)
	    //   })
	    // }
	    // );
	    return this.tempColumns;
  	}

  	extractVisibleColumnsIn(categoryNode: any, level: number) {
	    let visibleAmount = 0;
	    if (categoryNode) {
	      if (categoryNode.visible) {
	        if (!this.tempColumns[level]) {
	          this.tempColumns[level] = [];
	        }
	
	        let childLevel = level + 1;
	        if (categoryNode.label === this.classificationService.rootLabel) {
	          childLevel = 0;
	        }
	        if (categoryNode.children) {
	          for (let child of categoryNode.children) {
	            visibleAmount += this.extractVisibleColumnsIn(child, childLevel);
	          }
	        }
	        if (categoryNode.label !== this.classificationService.rootLabel) {
	          this.tempColumns[level].push({
	            label: categoryNode.label,
	            childrenAmount: visibleAmount
	          });
	          if (!categoryNode.children) {
	            visibleAmount += 1;
	          }
	        }
	      }
	    }
    	return visibleAmount;
  	}

}
