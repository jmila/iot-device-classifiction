export interface PaperClassification {
    id?: string;
    uniqueName?: string;
    data?: any[];
}