import { Injectable } from '@angular/core';
import * as FileSaver from 'file-saver';

@Injectable({
  providedIn: 'root'
})
export class FileUtilsService {

  constructor() { }

  public static saveAsJSONFile(json: any, fileName: string): void {
    var jsonData = JSON.stringify(json);
    const data: Blob = new Blob([jsonData], {
      type: "application/json"
    });
    FileSaver.saveAs(data, fileName + '.json');
  }

  public static saveAsTXTFile(txt: any, fileName: string): void {
    const data: Blob = new Blob([txt], {
      type: "text"
    });
    FileSaver.saveAs(data, fileName);
  }

  public static saveAsCSVFile(csv: any[], fileName: string): void {
    let csvData = "";
    csv.forEach(function (rowArray) {
      let row = rowArray.join(",");
      csvData += row + "\r\n";
    });
    const data: Blob = new Blob([csvData], {
      type: "text/csv;charset=utf-8;"
    });
    FileSaver.saveAs(data, fileName + '.csv');
  }

  // exportExcel() {
  //   import("xlsx").then(xlsx => {
  //     const worksheet = xlsx.utils.json_to_sheet(this.papersClassifications);
  //     const workbook = { Sheets: { data: worksheet }, SheetNames: ["data"] };
  //     const excelBuffer: any = xlsx.write(workbook, {
  //       bookType: "xlsx",
  //       type: "array"
  //     });
  //     this.saveAsExcelFile(excelBuffer, "products");
  //   });
  // }

  public static saveAsExcelFile(buffer: any, fileName: string): void {
    let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
    let EXCEL_EXTENSION = '.xlsx';
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });
    FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
  }

}
