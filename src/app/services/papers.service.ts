import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { parseBibFile, normalizeFieldValue, BibFilePresenter } from "bibtex";
import { Subject, Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PapersService {

  bibDataFormat: string = 'SPLNCS03';
  bibDataFormatSubject = new Subject<string>();
  bibFile!: BibFilePresenter;
  bibFileSubject = new Subject<BibFilePresenter>();

  httpSub !: Subscription;

  papersUniqueName: { [bibId: string]: string } = {}; // bibtex id -> displayed name (uniques)

  constructor(public http: HttpClient) {
    this.initialize()
  }

  emitBibDataFormat() {
    this.bibDataFormatSubject.next(this.bibDataFormat);
  }

  setBibDataFormat(format: string) {
    this.bibDataFormat = format;
    this.emitBibDataFormat();
  }

  initialize(): void {
    this.httpSub = this.http.get('./assets/papers.bib', { responseType: 'text' }).subscribe(res => {
      this.onReceiveBibFile(res);
      if (this.httpSub) {
        this.httpSub.unsubscribe();
      }
    });
  }

  cleanBibFile(bibFileContent: string) {
    // console.log("cleanBibFile: " + bibFileContent);
    bibFileContent = bibFileContent.replace(/[\n\r][ \t\w]*%.*[\n\r]/g, "");
    let bibEntryIdPattern = /@\w+{([^\n\r,]+),/g;

    let newBibFileContent: string = "";
    let lastIndex = 0;
    let ids: string[] = [];
    let removePrev = false;

    let match = bibEntryIdPattern.exec(bibFileContent);
    while (match != null) {
      if (removePrev) {
        removePrev = false;
      } else {
        newBibFileContent += bibFileContent.substring(lastIndex, match.index);
      }
      lastIndex = match.index;

      if (!ids.includes(match[1])) {
        ids.push(match[1]);
      } else {
        // must be removed
        console.log("cleanBibFile: removed duplicate: " + match[1]);
        removePrev = true;
      }

      match = bibEntryIdPattern.exec(bibFileContent);
    }

    if (removePrev) {
      removePrev = false;
    } else {
      newBibFileContent += bibFileContent.substring(lastIndex);
    }

    return newBibFileContent.replace(/=[ \t]*([^{}"\n\r]+),/g, "= {$1},").replace(/([\n\r][ \t\w]*)=[ \t]*([^{"\n\r]+)}/g, "$1 = {$2}").replace(/([\n\r][ \t\w]*)=[ \t]*{[ \t]*([^}"\n\r]+),([ \t]*[\n\r])/g, "$1 = {$2},$3"); //.replace(/\s{2}/, "");
  }

  onReceiveBibFile(data: string): void {
    this.bibFile = parseBibFile(data);
    console.log("create papers unique name...");
    this.createPapersUniqueName(this.bibFile);
    console.log("created papers unique name");
    this.emitBibFile();
  }

  emitBibFile() {
    this.bibFileSubject.next(this.bibFile);
  }

  getIds(bibFile: BibFilePresenter) {
    if (bibFile) {
      return Object.keys(bibFile.entries$);
    } else {
      return null;
    }
  }

  getAuthors(bibAuthors: string | undefined): string[] {
    if (!bibAuthors || bibAuthors.length <= 0) {
      return [];
    }
    return bibAuthors.split(' and ');
  }

  formatAuthors(authors: string | undefined): string {
    const authorsName = this.getAuthors(authors);
    if (authorsName.length === 0) {
      return "";
    }

    var result = "";
    authorsName.forEach((authorName) => {
      result = this.formatAuthor(result, authorName);
    });
    return result;
  }

  formatFirstAuthor(authors: string | undefined): string {
    const authorsName = this.getAuthors(authors);
    if (authorsName.length === 0) {
      return "";
    } else {
      return this.formatAuthor("", authorsName[0]);
    }
  }

  formatAuthor(result: string, authorName: string) {
    if (result.length != 0) {
      result += ", ";
    }
    var authorNames = authorName.split(",");
    if (authorNames.length >= 2) {
      result += authorNames[0];
      var namePattern = /(?<=[A-Z])(.+)/g;
      result += ", " + authorNames[1].replace(namePattern, ".");
    } else {
      authorNames = authorName.split(" ");
      if (authorNames.length >= 2) {
        result += authorNames[1] + ", " + authorNames[0];
      } else {
        result += authorName;
      }
    }
    return result;
  }

  getPaperUniqueNameBase(bibFile: BibFilePresenter, id: string): string {
    let bib = bibFile.getEntry(id);
    if (bib) {
      let author = this.formatFirstAuthor(bib.getFieldAsString("author") + "");
      let year = bib.getFieldAsString("year") + "";
      return author + " (" + year + ")";
    } else {
      return "MISSING_BIBTEX_ID";
    }
  }

  convertIntToLetter(i: number) {
    let letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p'];
    return i < letters.length ? letters[i] : (i + "");
  }

  createPapersUniqueName(bibFile: BibFilePresenter) {
    const papersId = this.getIds(bibFile);
    if (papersId == null) {
      return;
    }

    let uniqueNameBaseAmount: any = {};
    for (let id of papersId) {
      let nameBase = this.getPaperUniqueNameBase(bibFile, id);
      if (!uniqueNameBaseAmount[nameBase]) {
        uniqueNameBaseAmount[nameBase] = { amount: 0 };
      }
      uniqueNameBaseAmount[nameBase].amount++;
    }

    for (let id of papersId) {
      let nameBase = this.getPaperUniqueNameBase(bibFile, id);
      if (!uniqueNameBaseAmount[nameBase] || uniqueNameBaseAmount[nameBase].amount == 1) {
        this.papersUniqueName[id] = nameBase;
      } else {
        if (!('curIndex' in uniqueNameBaseAmount[nameBase])) {
          uniqueNameBaseAmount[nameBase].curIndex = 0;
        }
        let curIndex = uniqueNameBaseAmount[nameBase].curIndex;
        this.papersUniqueName[id] = nameBase + " " + this.convertIntToLetter(curIndex);
        uniqueNameBaseAmount[nameBase].curIndex++;
      }
    }
  }

  getPaperUniqueName(id: string) {
    let name = this.papersUniqueName[id];
    return name ? name : id;
  }

}
