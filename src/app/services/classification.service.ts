import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { PaperClassification } from '../models/paperclassification.model';
import { FileUtilsService } from './file-utils.service';
import { PapersService } from './papers.service';

@Injectable({
  providedIn: 'root'
})
export class ClassificationService {

  rootLabel = "Root";
  categoriesSubject = new Subject<any>();
  categories = {
    label: this.rootLabel,
    visible: true,
    children: [
      {
        label: 'Input: The classified devices',
        visible: true,
        children: [
          {
            label: 'Only IoT devices',
            visible: true,
            dataIndex: 3
          },
          {
            label: 'Both IoT and non IoT devices',
            visible: true,
            dataIndex: 4
          }
        ]
      },
      {
        label: 'Data acquisition',
        visible: true,
        children: [
          {
            label: 'Acquisition method',
            visible: true,
            children: [
              {
                label: 'Use of public dataset',
                visible: true,
                dataIndex: 2
              },
              {
                label: 'Collect data',
                visible: true,
                children: [
                  {
                    label: 'Share publicly',
                    visible: true,
                    dataIndex: 1
                  },
                  {
                    label: 'Keep private',
                    visible: true,
                    dataIndex: 0
                  }]
              }
            ]
          },
          {
            label: 'Traffic type',
            visible: true,
            children: [
              {
                label: 'Setup',
                visible: true,
                dataIndex: 5
              }, {
                label: 'Idle',
                visible: true,
                dataIndex: 6
              }, {
                label: 'Interaction',
                visible: true,
                dataIndex: 7
              }
            ]
          },
          {
            label: 'Capture point',
            visible: true,
            children: [
              {
                label: 'At the gateway',
                visible: true,
                dataIndex: 8
              }, {
                label: 'After the gateway',
                visible: true,
                dataIndex: 9
              }
            ]
          }
        ]
      },
      {
        label: 'Features',
        visible: true,
        children: [
          {
            label: 'Packet level features',
            visible: true,
            children: [
              {
                label: 'Header',
                visible: true,
                dataIndex: 16
              },
              {
                label: 'Payload',
                visible: true,
                dataIndex: 17
              }
            ]
          },
          {
            label: 'Stream level features',
            visible: true,
            children: [
              {
                label: 'Stream definition',
                visible: true,
                children: [
                  {
                    label: 'Finite sequence of N packets',
                    visible: true,
                    dataIndex: 13
                  },
                  {
                    label: 'Packets exchanged within a time period',
                    visible: true,
                    dataIndex: 14
                  }, {
                    label: 'A flow',
                    visible: true,
                    dataIndex: 15
                  }
                ]
              },
              {
                label: 'Stream features',
                visible: true,
                children: [
                  {
                    label: 'Volume features',
                    visible: true,
                    dataIndex: 18
                  },
                  {
                    label: 'Protocol features',
                    visible: true,
                    dataIndex: 19
                  },
                  {
                    label: 'Time related features',
                    visible: true,
                    dataIndex: 20
                  },
                  {
                    label: 'Periodicity features',
                    visible: true,
                    dataIndex: 21
                  }
                ]
              }
            ]
          },
          {
            label: 'Dimensionality reduction',
            visible: true,
            dataIndex: 22
          },
          {
            label: 'Deep learning based feature extraction',
            visible: true,
            dataIndex: 23
          }
        ]
      },
      {
        label: 'Classifiers',
        visible: true,
        children: [
          {
            label: 'Number of classifiers',
            visible: true,
            children: [
              {
                label: 'One-class classifier',
                visible: true,
                dataIndex: 24
              },
              {
                label: 'Multi-class classifier',
                visible: true,
                dataIndex: 25
              }
            ]
          },
          {
            label: 'Availability of labeled data',
            visible: true,
            children: [
              {
                label: 'Supervised learning',
                visible: true,
                dataIndex: 26
              },
              {
                label: 'Unsupervised learning',
                visible: true,
                dataIndex: 27
              }
            ]
          },
          {
            label: 'Depth of classifiers',
            visible: true,
            children: [
              {
                label: 'Deep learning',
                visible: true,
                dataIndex: 28
              },
              {
                label: 'Shallow learning',
                visible: true,
                dataIndex: 29
              }
            ]
          },
          {
            label: 'Evaluation metrics',
            visible: true,
            dataIndex: 30
          }
        ]
      },
      {
        label: 'Output: the classification granularity',
        visible: true,
        children: [
          {
            label: 'Device category',
            visible: true,
            dataIndex: 12
          }, {
            label: 'Device type',
            visible: true,
            dataIndex: 11
          },
          {
            label: 'Device instance',
            visible: true,
            dataIndex: 10
          }
        ]
      }
    ]
  };

  visibleLeafCategoriesDataIndex: number[] = [];

  constructor(private http: HttpClient, private papersService: PapersService) {
    this.updateVisibleLeafCategoriesIndex();
  }

  getVisibleLeafCategoriesDataIndex() {
    return this.visibleLeafCategoriesDataIndex;
  }
  getVisibleLeafCategories() {
    //    console.log("VisibleLeafIndex: !!");
    // return [0, 1, 2, 3, 4];
    let start = Date.now()
    let res = this.getVisibleLeafCategoriesFrom(this.categories, []);
    if (res.length == 0) {
      //      console.log("VISI RES: empty")
    } else {
      //      res.forEach((t) => console.log("vl index: " + t));
    }
    let end = Date.now() - start;
    //    console.log("VisibleLeafIndex TIME: " + end)
    return res;
  }

  getVisibleLeafCategoriesFrom(categoryNode: any, result: number[]): number[] {
    //    console.log("VisiLeaf:  " + categoryNode.label + " , " + categoryNode.visible + ", " + this.categories.visible);
    if (categoryNode && categoryNode.visible) {
      //      console.log("VisiLeaf: if " + categoryNode.label);
      if (categoryNode.children) {
        //        console.log("VisiLeaf: children " + categoryNode.label);
        for (let child of categoryNode.children) {
          this.getVisibleLeafCategoriesFrom(child, result);
        }
      } else {
        //        console.log("VisiLeaf: else " + categoryNode.label);
        if (categoryNode.label !== undefined) {
          result.push(categoryNode.label);
        }
      }

    }
    if (result.length == 0) {
      //      console.log("VisiLeaf: empty " + categoryNode.label);
    }
    return result;
  }

  updateVisibleLeafCategoriesIndex() {
    console.log("updateVisibleLeafCategoriesIndex()");
    let start = Date.now()
    let res = this.updateVisibleLeafCategoriesIndexFrom(this.categories, []);
    // if (res.length == 0) {
    //   console.log("VISI RES: empty")
    // } else {
    //   res.forEach((t) => console.log("vl index: " + t));
    // }
    let end = Date.now() - start;
    console.log("updateVisibleLeafCategoriesIndex TIME: " + end)
    this.visibleLeafCategoriesDataIndex = res;
  }

  updateVisibleLeafCategoriesIndexFrom(categoryNode: any, result: number[]): number[] {
    if (categoryNode && categoryNode.visible) {
      if (categoryNode.children) {
        for (let child of categoryNode.children) {
          this.updateVisibleLeafCategoriesIndexFrom(child, result);
        }
      } else {
        if (categoryNode.dataIndex !== undefined) {
          result.push(categoryNode.dataIndex);
        }
      }
    }
    if (result.length == 0) {
      console.log("VisiLeaf: empty " + categoryNode.label);
    }
    return result;
  }

  emitCategories() {
    this.categoriesSubject.next(this.categories);
  }

  setCategoryVisiblityState(categoryName: string, state: boolean) {
    this.setCategoryVisiblityStateIn(categoryName, state, this.categories);
    this.updateVisibleLeafCategoriesIndex();
  }

  setCategoryVisiblityStateIn(categoryName: string, state: boolean, categoryNode: any) {
    if (categoryNode) {
      if (categoryNode.label === categoryName) {
        categoryNode.visible = state;
        // console.log("setCategoVisi: " + state + ", " + categoryName)
        return true;
      }

      if (categoryNode.children) {
        for (let child of categoryNode.children) {
          if (this.setCategoryVisiblityStateIn(categoryName, state, child)) {
            return true;
          }
        }
      }

    }

    return false;
  }

  getCategory(categoryName: string) {
    // console.log("GetCatego: " + this.getCategoryIn(categoryName, this.categories).label)
    return this.getCategoryIn(categoryName, this.categories);
  }

  getCategoryIn(categoryName: string, categoryNode: any): any {
    if (categoryNode) {
      if (categoryNode.label === categoryName) {
        return categoryNode;
      }

      if (categoryNode.children) {
        for (let child of categoryNode.children) {
          let category = this.getCategoryIn(categoryName, child);
          if (category) {
            return category;
          }
        }
      }

    }

    return null;
  }

  dbNames: any = {
    "miettinen2017iot": "Sentinel",
    "sivanathan2017characterizing": "UNSW 2017",
    "sivanathan2018classifying": "UNSW 2018",
    "alrawi2019sok": "YourThings",
    "perdisci2020iotfinder": "IoTFinder",
    "cicvitic2021ensemble": "SHIoT",
    "ren2019information": "MON(IOT)R",
    "dong2020your": "HomeMole",
    "meidan2020novel": "IoT-deNAT",
    "cigarcia2020IoT23": "IoT-23",
    "kolcun2021revisiting": "DADABox",
  }

  renameDb(dataValue: string) {
    if (typeof dataValue === 'string') {
      let values = dataValue.split(',');
      let newDataValue = "";
      for (let val of values) {
        val = val.trim();
        if (this.dbNames[val]) {
          if (newDataValue.length > 0) {
            newDataValue += ", ";
          }
          newDataValue += this.dbNames[val];
        }
      }
      if (newDataValue.length > 0) {
        return newDataValue;
      }
    }

    return dataValue;
  }

  getPapersClassification() {
    return this.http.get<any>('assets/papers-classification.json')
      .toPromise()
      .then(res => {
        let result: PaperClassification[] = [];
        for (let paperId in res.data) {
          let paperClassif = { id: paperId, uniqueName: this.papersService.getPaperUniqueName(paperId), data: res.data[paperId] };
          for (let i = 0; i < paperClassif.data.length; i++) {
            paperClassif.data[i] = this.renameDb(paperClassif.data[i]);
          }
          result.push(paperClassif);
        }

        return result;
        // return <PaperClassification[]>res.data
      }) //TODO change data to suit paperclassif
      .then(data => { return data; });
  }

  getPapersClassificationInRange(minIndex: number, maxIndex: number) {
    return this.http.get<any>('assets/papers-classification.json')
      .toPromise()
      .then(res => {
        let result: PaperClassification[] = [];
        for (let paperId in res.data) {
          let paperClassif = { id: paperId, uniqueName: this.papersService.getPaperUniqueName(paperId), data: res.data[paperId].slice(minIndex, maxIndex + 1) };
          result.push(paperClassif);
        }
        return result;
        // return <PaperClassification[]>res.data
      }) //TODO change data to suit paperclassif
      .then(data => { return data; });
  }

  getPapersClassificationOfTable(tableIndex: number) {
    if (tableIndex == 1) {
      return this.getPapersClassificationInRange(0, 12);
    }
    if (tableIndex == 2) {
      return this.getPapersClassificationInRange(13, 23);
    }
    if (tableIndex == 3) {
      return this.getPapersClassificationInRange(24, 30);
    }
    return null;
  }

  extractTablesRows(latexData: string): string[] {
    // latexData = latexData.replace(/^%.*$/g, ""); //Remove comments, split rows

    let rows: string[] = [];
    for (let line of latexData.split('\\\\')) {
      var paperClassificationRowPattern = /(.|\n|\r){1,100}\\cite{(.+)}\s*&/g;
      let match = paperClassificationRowPattern.exec(line);
      if (match != null) {
        rows.push(line);
      } else {
        console.log("extractTablesRows: ignore " + line);
      }
    }

    console.log("Total tables rows: " + rows.length);
    return rows;
  }

  parseLatexToJs(data: string) {
    let res: any = { data: {} };

    const papersClassifsRows = this.extractTablesRows(data);
    let debug = true;

    papersClassifsRows.forEach((taxoTableRow) => {
      let paperId = null;
      let paperData: any[] = [];

      const rowCellsData = taxoTableRow.split('&');
      for (let rowCellData of rowCellsData) {
        rowCellData = rowCellData.trim();
        var paperClassificationDataPattern = /(\\cite{(.+)}|\\ding{(53)}|\\ding{(51)})/g;
        let match = paperClassificationDataPattern.exec(rowCellData);
        if (match != null) {
          // matched text: match[0]
          // match start: match.index
          // capturing group n: match[n]
          let papdata = null;
          for (let i = 0; i < match.length; i++) {
            if (match[i]) {
              if (i == 2 && !paperId) {
                paperId = match[i];
                if (paperId === 'meidan2017detection') {
                  debug = true;
                  console.log("parseLatexToJs: row = " + taxoTableRow);
                  console.log("parseLatexToJs: rowCell = " + rowCellData);
                }
              } else if (i >= 2) {
                papdata = match[i];
              }
            }
          }
          if (papdata) {
            if (papdata === "51") {
              papdata = 1;
            } else if (papdata === "53") {
              papdata = 0;
            }
            paperData.push(papdata);
            if (debug) {
              console.log("parseLatexToJs: pushed data = " + papdata);
            }
          }

          // match = paperClassificationDataPattern.exec(rowCellData);
        } else {
          paperData.push(rowCellData); // str
          if (debug) {
            console.log("parseLatexToJs: pushed = " + rowCellData);
          }
        }
      }

      if (paperId) {
        if (res.data[paperId]) {
          res.data[paperId] = res.data[paperId].concat(paperData);
          if (debug) {
            console.log("parseLatexToJs: concated for " + paperId + " : " + paperData);
          }
        } else {
          res.data[paperId] = paperData;
          if (debug) {
            console.log("parseLatexToJs: set " + paperId + " : " + paperData);
          }
        }
      }
      debug = false;
    });

    return res;
  }

}
