import { Component, OnInit } from '@angular/core';
import { OrganizationChartModule } from 'primeng/organizationchart';
import { TreeNode } from 'primeng/api';
import { ClassificationService } from '../services/classification.service';
import { ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'app-classification-overview',
  templateUrl: './classification-overview.component.html',
  styleUrls: ['./classification-overview.component.scss']
})
export class ClassificationOverviewComponent implements OnInit {

  data!: TreeNode[];
  refresher: boolean = true;

  constructor(public classificationService: ClassificationService, private ref: ChangeDetectorRef) { }

  extractTreeNode(categoryNode: any): any {
    if (categoryNode) {
      let res: any = {};
      res.label = categoryNode.label;
      res.expanded = res.label === this.classificationService.rootLabel;
      if (categoryNode.children) {
        for (let child of categoryNode.children) {
          let childTreeNode = this.extractTreeNode(child);
          if (childTreeNode) {
            if (!res.children) {
              res.children = [];
            }
            res.children.push(childTreeNode);
          }
        }
      }
      return res;
    } else {
      return null;
    }
  }

  ngOnInit() {
    this.data = [this.extractTreeNode(this.classificationService.categories)];
    // this.recursiveSetSwitchesState(true, this.data[0]);
  }

  onNodeExpandButtonClick(event: any) {
    console.log("Node Expand but 0 ");
    if (event.node) {
      console.log("Node Expand but");
      if (this.data[0].children) {
        let isExpandedNodeATopCategory = false;
        for (let child of this.data[0].children) {
          if (child.label === event.node.label) {
            isExpandedNodeATopCategory = true;
            break;
          }
        }
        if (isExpandedNodeATopCategory) {
          for (let child of this.data[0].children) {
            if (child.label !== event.node.label) {
              child.expanded = false;
            }
          }
          this.refresh();
        }
      }
    }
  }

  onExpandAllButtonClick(expandAll: boolean) {
    console.log("Clicked expand button value: " + expandAll);
    this.recursiveSetExpanded(this.data[0], expandAll);
    this.refresh();
  }

  recursiveSetExpanded(node: TreeNode, state: boolean) {
    if (node) {
      if (node.label !== this.classificationService.rootLabel) {
        node.expanded = state;
      }
      if (node.children) {
        for (let child of node.children) {
          this.recursiveSetExpanded(child, state);
        }
      }
    }
  }

  refresh() {
    this.refresher = false;
    setTimeout(() => this.refresher = true, 0);
  }

  onNodeSwitchStateChange(node: TreeNode, event: any) {
    console.log("Clicked " + node.label + ": " + event.checked);
    this.recursiveSetSwitchesState(event.checked, node);
    let parentNode = this.getNodeParent(node);
    if (parentNode) {
      this.antiRecursiveSetSwitchedState(parentNode);
    }
    node.expanded = event.checked;
    this.classificationService.emitCategories();
  }

  recursiveSetSwitchesState(state: boolean, node: TreeNode) {
    if (node) {
      if (node.label) {
        this.classificationService.setCategoryVisiblityState(node.label, state);
      }
      if (node.children) {
        node.children.forEach((child) => {
          this.recursiveSetSwitchesState(state, child)
        });
      }
      // let parentNode = this.getNodeParent(node);
      // if (parentNode) {
      //   this.antiRecursiveSetSwitchedState(parentNode);
      // }
    }
  }

  getNodeParent(node: TreeNode): TreeNode | undefined {
    return this.getNodeParentFrom(node, this.data[0]);
  }

  getNodeParentFrom(node: TreeNode, curNode: TreeNode): TreeNode | undefined {
    if (curNode) {
      if (curNode.children) {
        for (let child of curNode.children) {
          if (child.label === node.label) {
            return curNode;
          } else {
            let childSearch = this.getNodeParentFrom(node, child);
            if (childSearch) {
              return childSearch;
            }
          }
        }

      }
    }
    return undefined;
  }

  antiRecursiveSetSwitchedState(node: TreeNode) {
    if (node) {
      if (node.children) {
        var allChildrenDisabled = true;
        for (let child of node.children) {
          if (child.label) {
            if (this.classificationService.getCategory(child.label).visible) {
              allChildrenDisabled = false;
            }
          }
        }
        if (node.label) {
          this.classificationService.setCategoryVisiblityState(node.label, !allChildrenDisabled);
          node.expanded = !allChildrenDisabled;
        }
      }
      let parentNode = this.getNodeParent(node);
      if (parentNode) {
        this.antiRecursiveSetSwitchedState(parentNode);
      }
    }
  }

}
