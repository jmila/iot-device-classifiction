import { Component, OnDestroy, OnInit } from '@angular/core';
import Two from 'two.js';
import { Anchor } from 'two.js/src/anchor';
import { Line } from 'two.js/src/shapes/line';
import { Vector } from 'two.js/src/vector';
import { Text } from 'two.js/src/text';
import { PaperClassification } from '../models/paperclassification.model';
import { ClassificationService } from '../services/classification.service';
import { Subscription } from 'rxjs';
import { PapersService } from '../services/papers.service';

@Component({
  selector: 'app-detailed-sankey',
  templateUrl: './detailed-sankey.component.html',
  styleUrls: ['./detailed-sankey.component.scss']
})
export class DetailedSankeyComponent implements OnInit, OnDestroy {

  papersClassifications!: PaperClassification[];
  visibleTopAndLeafCategories !: any[]; // [{top: <top category label>, leaves: [{ label: <leaf 1 label>, linksAmount: <amount of links going through leaf 1> }, <leaf 2 ...>]}}]
  allLeavesLinksAmount !: any[]; // at visible leaf category index, get the amount of links
  // visibleLeavesLinksAmount !: number[];

  twoInstance!: Two;
  notHoveredColor: string = "#c5c6c7";
  hoverText: Text | null = null;

  categoriesSub !: Subscription;

  showNonHighlightedLinks = true;
  selectedPapersClassifications!: PaperClassification[];

  constructor(public classificationService: ClassificationService, private papersService: PapersService) { }

  ngOnInit(): void {
    this.classificationService.getPapersClassification()?.then(data => {
      this.papersClassifications = data;

      this.allLeavesLinksAmount = this.calculateLeavesLinksAmount();
      console.log("calculateLeavesLinksAmount: result: " + this.allLeavesLinksAmount);

      this.classificationService.emitCategories();
    });

    this.categoriesSub = this.classificationService.categoriesSubject.subscribe((categories) => {
      this.visibleTopAndLeafCategories = this.extractVisibleTopAndLeafCategories(categories);
      console.log("extractVisibleTopAndLeafCategories: result: " + JSON.stringify(this.visibleTopAndLeafCategories));

      // this.visibleLeavesLinksAmount = this.getVisibleLeavesLinksAmount(this.allLeavesLinksAmount);
      // console.log("getVisibleLeavesLinksAmount: result: " + this.visibleLeavesLinksAmount);

      this.drawSankey();
    });
  }

  ngOnDestroy(): void {
    this.categoriesSub.unsubscribe();
  }

  onSelectedPapersChange() {
    setTimeout(() => this.drawSankey(), 10);
  }

  onButtonShowNonHoveredLinks(e: any) {
    if (e.checked) {
      this.notHoveredColor = "#c5c6c7";
    } else {
      this.notHoveredColor = "#ffffff";
    }
  }

  drawSankey() {
    // if (!this.visibleLeavesLinksAmount) {
    //   return;
    // }
    if (this.twoInstance) {
      this.twoInstance.clear();
    } else {
      var params = {
        fitted: true
      };
      var elem = document.getElementById("sankey-diagram");
      if (elem == null) {
        console.log("sankey not found");
        elem = document.body;
      }
      this.twoInstance = new Two(params).appendTo(elem);
    }

    var columnsAmount = this.visibleTopAndLeafCategories.length + 1; // + 1 for papers column

    var spaceWidthCoef = 0.8;
    //two.width = categoryWidth * columnsAmount + spaceWidthCoef * categoryWidth * columnsAmount;
    var categoryWidth = this.twoInstance.width / (columnsAmount * (1 + spaceWidthCoef));
    var spaceBetweenCategoriesWidth = categoryWidth * spaceWidthCoef;
    console.log("drawSankey: categoryWidth: " + categoryWidth + ", spaceBetweenCategoriesWidth:" + spaceBetweenCategoriesWidth);

    // let line = this.twoInstance.makeLine(this.twoInstance.width, 0, this.twoInstance.width, this.twoInstance.height);
    // line.linewidth = 5;
    // line.stroke = "rgba(255, 0, 0, 0.5)";

    let topHeight = 30;
    let spaceBetweenLeaves = 30;
    let categoryColor = 'rgb(0, 200, 255)';
    let categoryOpacity = 0.75;

    let columnIndex = 1; // 0 for papers
    let leafIndex = 0;
    for (let visibleTopAndLeafCategory of this.visibleTopAndLeafCategories) {
      let columnX = (categoryWidth * 0.5) + columnIndex * (spaceBetweenCategoriesWidth + categoryWidth);
      this.twoInstance.makeText(visibleTopAndLeafCategory.top, columnX, 10);

      // let line = this.twoInstance.makeLine(columnX, 0, columnX, this.twoInstance.height);
      // line.linewidth = 1;
      // line.stroke = "rgba(0, 255, 0, 0.5)";

      let totalLinksAmount = 0;
      let leavesAmount = visibleTopAndLeafCategory.leaves.length;

      for (let leafCategory of visibleTopAndLeafCategory.leaves) {
        totalLinksAmount += leafCategory.linksAmount;
      }
      // leafIndex -= leavesAmount;

      let leavesSpace = this.twoInstance.height - topHeight - leavesAmount * spaceBetweenLeaves;
      let leafY = topHeight;

      for (let leafCategory of visibleTopAndLeafCategory.leaves) {
        // let leafLinksAmount = this.visibleLeavesLinksAmount[leafIndex];
        let leafLinksAmount = leafCategory.linksAmount;
        let leafHeight = leavesSpace * (leafLinksAmount / totalLinksAmount);
        let leafInputOutputY = leafY;
        leafY += (leafHeight * 0.5);

        var rect = this.twoInstance.makeRectangle(columnX, leafY, categoryWidth, leafHeight);

        rect.fill = visibleTopAndLeafCategory.color;
        rect.opacity = categoryOpacity;

        let leafTitle = this.twoInstance.makeText(leafCategory.label, columnX, leafY);
        let leafInputX = columnX - (categoryWidth * 0.5);
        let leafOutputX = columnX + (categoryWidth * 0.5);
        let spaceBetweenLinks = leafHeight / leafLinksAmount;
        leafInputOutputY += spaceBetweenLinks * 0.5;

        leafCategory.inputX = leafInputX;
        leafCategory.inputOutputY = leafInputOutputY;
        leafCategory.spaceBetweenLinks = spaceBetweenLinks;
        leafCategory.freeLinkIndex = 0;

        for (let inputIndex = 0; inputIndex < leafLinksAmount; inputIndex++) {
          this.twoInstance.makeCircle(leafInputX, leafInputOutputY, 1.5).fill = 'rgb(0,0,0)';
          this.twoInstance.makeCircle(leafOutputX, leafInputOutputY, 1.5).fill = 'rgb(0,0,0)';
          leafInputOutputY += spaceBetweenLinks;
        }

        leafY += (leafHeight * 0.5) + spaceBetweenLeaves;
        leafIndex++;
      }

      columnIndex++;
    }

    // Links drawing
    let paperStartY = topHeight;
    let spaceBetweenPapers = this.selectedPapersClassifications ? (this.twoInstance.height - paperStartY) / this.selectedPapersClassifications.length : 0;
    let paperStartX = categoryWidth * 0.5;
    let totalPapersAmount = this.selectedPapersClassifications ? this.selectedPapersClassifications.length : this.papersClassifications.length;

    let paperIndex = 0;
    let links: any = {};
    if (this.selectedPapersClassifications) {
      for (let paperClassif of this.selectedPapersClassifications) {
        let prevX = paperStartX;
        let prevY = paperStartY;
        if (!paperClassif.id) {
          continue;
        }
        if (paperClassif.data) {
          let paperColor = this.getRandColor(totalPapersAmount, paperIndex);
          for (let visibleTopAndLeafCategory of this.visibleTopAndLeafCategories) {
            for (let leaf of visibleTopAndLeafCategory.leaves) {
              let leafDataIndex = leaf.dataIndex;
              if ((paperClassif.data[leafDataIndex] == 1) || (paperClassif.data[leafDataIndex] !== 0 && typeof paperClassif.data[leafDataIndex] === 'string' && paperClassif.data[leafDataIndex].includes(leaf.label))) {
                let leafInputLinkIndex = leaf.freeLinkIndex;
                let nextX = leaf.inputX;
                let nextY = leaf.inputOutputY + leafInputLinkIndex * leaf.spaceBetweenLinks;

                let linkLine = this.twoInstance.makeLine(prevX, prevY, nextX, nextY);
                linkLine.stroke = paperColor;

                if (!links[paperClassif.id]) {
                  links[paperClassif.id] = [];
                }
                links[paperClassif.id].push(linkLine);

                prevX = nextX + categoryWidth;
                prevY = nextY;
                leaf.freeLinkIndex++;
              }
            }
          }
        } else {
          console.log("No data for paper class id: " + paperClassif.id);
        }

        paperStartY += spaceBetweenPapers;
        paperIndex++;
      }

      this.twoInstance.update();

      for (let paperId in links) {
        for (let link of links[paperId]) {
          let inst: DetailedSankeyComponent = this;
          (<any>link._renderer).elem.addEventListener('mouseover', function () {
            // console.log("Hover on " + paperId);
            let firstLink = links[paperId][0];
            inst.hoverText = inst.twoInstance.makeText(inst.papersService.getPaperUniqueName(paperId), firstLink.vertices[0].x, firstLink.vertices[0].y + 10);
            for (let link of links[paperId]) {
              link.linewidth = 4;
            }
            for (let otherPaperId in links) {
              if (otherPaperId === paperId) {
                continue;
              }
              for (let link of links[otherPaperId]) {
                if (inst.notHoveredColor === "#ffffff") {
                  link.visible = false;
                } else {
                  link.previousStroke = link.stroke;
                  link.stroke = inst.notHoveredColor;
                }
              }
            }
            inst.twoInstance.update();
          }, false);

          (<any>link._renderer).elem.addEventListener('mouseout', function () {
            // console.log("Unhover on " + paperId);
            if (inst.hoverText !== null) {
              inst.hoverText.remove();
            }
            for (let link of links[paperId]) {
              link.linewidth = 1;
            }
            for (let otherPaperId in links) {
              if (otherPaperId === paperId) {
                continue;
              }
              for (let link of links[otherPaperId]) {
                link.visible = true;
                if (link.previousStroke) {
                  link.stroke = link.previousStroke;
                  delete link.previousStroke;
                }
              }
            }
            inst.twoInstance.update();
          }, false);
        }
      }
    } else {
      this.twoInstance.update();
    }
  }

  getRandomColor(): string {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

  /**
 * @param numOfSteps: Total number steps to get color, means total colors
 * @param step: The step number, means the order of the color
 */
  getRandColor(numOfSteps: number, step: number) {
    // This function generates vibrant, "evenly spaced" colours (i.e. no clustering). This is ideal for creating easily distinguishable vibrant markers in Google Maps and other apps.
    // Adam Cole, 2011-Sept-14
    // HSV to RBG adapted from: http://mjijackson.com/2008/02/rgb-to-hsl-and-rgb-to-hsv-color-model-conversion-algorithms-in-javascript
    var r, g, b;
    var h = step / numOfSteps;
    var i = ~~(h * 6);
    var f = h * 6 - i;
    var q = 1 - f;
    switch (i % 6) {
      case 0: r = 1; g = f; b = 0; break;
      case 1: r = q; g = 1; b = 0; break;
      case 2: r = 0; g = 1; b = f; break;
      case 3: r = 0; g = q; b = 1; break;
      case 4: r = f; g = 0; b = 1; break;
      case 5: r = 1; g = 0; b = q; break;
    }
    if (!r) {
      r = 0;
    }
    if (!g) {
      g = 0;
    }
    if (!b) {
      b = 0;
    }
    var c = "#" + ("00" + (~ ~(r * 255)).toString(16)).slice(-2) + ("00" + (~ ~(g * 255)).toString(16)).slice(-2) + ("00" + (~ ~(b * 255)).toString(16)).slice(-2);
    return (c);
  }

  /**
   * 
   * @return links amount per leaves for all leaves, visible or not, ordered by dataIndex
   */
  calculateLeavesLinksAmount(): number[] {
    let linksAmount: any[] = [];
    for (let i = 0; i < 70; i++) { // 70 is higher than necessary
      linksAmount[i] = 0;
    }

    for (let paperClassif of this.papersClassifications) {
      if (paperClassif.data) {
        for (let dataIndex in paperClassif.data) {
          let dataValue = paperClassif.data[dataIndex];
          if (dataValue === 1) {
            linksAmount[dataIndex]++;
          } else if (dataValue !== 0 && typeof dataValue === 'string') {
            let values = dataValue.split(",");
            for (let value of values) {
              dataValue = value.trim();
              if (linksAmount[dataIndex] === 0) {
                linksAmount[dataIndex] = {};
              }
              if (!(dataValue in linksAmount[dataIndex])) {
                linksAmount[dataIndex][dataValue] = 0;
              }
              linksAmount[dataIndex][dataValue]++;
            }
          }
        }
      }
    }
    return linksAmount;
  }

  extractVisibleTopAndLeafCategories(categories: any): any[] {
    let tempCategories: any[] = [];

    let categoryNode = categories;
    if (categoryNode) {
      if (categoryNode.visible) {
        if (categoryNode.label === this.classificationService.rootLabel) {
          for (let topIndex = 0; topIndex < categoryNode.children.length; topIndex++) {
            let top = categoryNode.children[topIndex];
            let sectionColor = this.getRandColor(categoryNode.children.length, topIndex);

            let leaves: any[] = [];
            this.extractVisibleLeafCategoriesIn(top, leaves);
            let leavesCpy: any[] = [];
            for (let leaf of leaves) {
              let linksAmountObject = this.allLeavesLinksAmount[leaf.dataIndex];
              if (typeof linksAmountObject === 'number') {
                leavesCpy.push({ label: leaf.label, dataIndex: leaf.dataIndex, linksAmount: <number>linksAmountObject });
                // leavesCpy.push({ label: leaf.label, dataIndex: leaf.dataIndex });
              } else {
                // this.leafCategoriesWithSubLeaves[leaf.label] = [];
                for (let catStrValue in linksAmountObject) {
                  // this.leafCategoriesWithSubLeaves[leaf.label].push({ label: catStrValue, visible: true });
                  leavesCpy.push({ label: catStrValue, dataIndex: leaf.dataIndex, linksAmount: <number>linksAmountObject[catStrValue] });
                }
              }
            }
            tempCategories.push({ top: top.label, leaves: leavesCpy, color: sectionColor });
          }
        } else {
          console.log("extractVisibleTopAndLeafCategories: problem, root not found");
        }
      }
    }

    return tempCategories;
  }

  extractVisibleLeafCategoriesIn(categoryNode: any, result: any[]) {
    if (categoryNode) {
      if (categoryNode.visible) {
        if (categoryNode.children) {
          for (let child of categoryNode.children) {
            this.extractVisibleLeafCategoriesIn(child, result);
          }
        } else {
          result.push(categoryNode);
        }
      }
    }
  }

}
