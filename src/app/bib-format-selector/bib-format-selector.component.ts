import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { SelectButtonModule } from 'primeng/selectbutton';
import { Subscription } from 'rxjs';
import { PapersService } from '../services/papers.service';

@Component({
  selector: 'app-bib-format-selector',
  templateUrl: './bib-format-selector.component.html',
  styleUrls: ['./bib-format-selector.component.scss']
})
export class BibFormatSelectorComponent implements OnInit, OnDestroy {

  formats: any[];

  selectedFormat: any;

  bibDataFormatSub !: Subscription;

  constructor(public papersService: PapersService) {
    this.formats = [
      'SPLNCS03',
      'APA',
      'MLA'
    ];
  }

  ngOnInit(): void {
    this.bibDataFormatSub = this.papersService.bibDataFormatSubject.subscribe((format) => {
      this.selectedFormat = format;
    })
    this.papersService.emitBibDataFormat();
  }

  ngOnDestroy(): void {
    this.bibDataFormatSub.unsubscribe();
  }

  onSelectedFormatChange(event: any) {
    this.papersService.setBibDataFormat(event.value);
  }

}
