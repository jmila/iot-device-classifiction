import { Component, OnInit } from '@angular/core';
import * as FileSaver from 'file-saver';
import { PaperClassification } from '../models/paperclassification.model';
import { ClassificationService } from '../services/classification.service';

@Component({
  selector: 'app-classification-table1',
  templateUrl: './classification-table1.component.html',
  styleUrls: ['./classification-table1.component.scss']
})
export class ClassificationTable1Component implements OnInit {

  papersClassifications!: PaperClassification[];

  cols!: any[];

  exportColumns!: any[];

  displayBib: boolean = false;
  displayPaperOfId: string = "";

  selectedBibFormat: string = 'SPLNCS03';

  constructor(private classificationService: ClassificationService) { }

  showDialog(paperId: string) {
    this.displayPaperOfId = paperId;
    this.displayBib = true;
  }

  ngOnInit() {
    this.classificationService.getPapersClassificationOfTable(1)?.then(data => (this.papersClassifications = data));

    this.cols = [
      // { field: "code", header: "Code" },
      { field: "c0", header: "c1" },
      { field: "c1", header: "c2" },
      { field: "c2", header: "c3" },
      { field: "c3", header: "c4" },
      { field: "c4", header: "c5" },
      { field: "c4", header: "c5" },
      { field: "c4", header: "c5" },
      { field: "c4", header: "c5" },
      { field: "c4", header: "c5" },
      { field: "c4", header: "c5" },
      { field: "c4", header: "c5" },
      { field: "c4", header: "c5" },
      { field: "c4", header: "c5" },
      { field: "c0", header: "c1" },
      { field: "c1", header: "c2" },
      { field: "c2", header: "c3" },
      { field: "c3", header: "c4" },
      { field: "c4", header: "c5" },
      { field: "c4", header: "c5" },
      { field: "c4", header: "c5" },
      { field: "c4", header: "c5" },
      { field: "c4", header: "c5" },
      { field: "c4", header: "c5" },
      { field: "c4", header: "c5" },
      { field: "c0", header: "c1" },
      { field: "c1", header: "c2" },
      { field: "c2", header: "c3" },
      { field: "c3", header: "c4" },
      { field: "c4", header: "c5" },
      { field: "c4", header: "c5" },
      { field: "c4", header: "c5" },
    ];

    this.exportColumns = this.cols.map(col => ({
      title: col.header,
      dataKey: col.field
    }));
  }

  // exportExcel() {
  //   import("xlsx").then(xlsx => {
  //     const worksheet = xlsx.utils.json_to_sheet(this.papersClassifications);
  //     const workbook = { Sheets: { data: worksheet }, SheetNames: ["data"] };
  //     const excelBuffer: any = xlsx.write(workbook, {
  //       bookType: "xlsx",
  //       type: "array"
  //     });
  //     this.saveAsExcelFile(excelBuffer, "products");
  //   });
  // }

  saveAsExcelFile(buffer: any, fileName: string): void {
    let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
    let EXCEL_EXTENSION = '.xlsx';
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });
    FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
  }

}
