import { HttpClient } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscriber, Subscription } from 'rxjs';
import { ClassificationService } from '../services/classification.service';
import { FileUtilsService } from '../services/file-utils.service';
import { PapersService } from '../services/papers.service';

@Component({
  selector: 'app-administration',
  templateUrl: './administration.component.html',
  styleUrls: ['./administration.component.scss']
})
export class AdministrationComponent implements OnInit, OnDestroy {

  sub !: Subscription;
  sub2 !: Subscription;

  constructor(public classificationService: ClassificationService, private http: HttpClient, private papersService: PapersService) { }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
    if (this.sub2) {
      this.sub2.unsubscribe();
    }
  }

  onParseLatexButtonClick() {
    console.log("button clicked");
    this.sub = this.http.get('./assets/papers-classification.tex', { responseType: 'text' }).subscribe(res => {
      this.onReceiveLatexFile(res);
      if (this.sub) {
        this.sub.unsubscribe();
      }
    });
  }

  onParseBibtexButtonClick() {
    console.log("button clicked");
    this.sub2 = this.http.get('./assets/papers.bib', { responseType: 'text' }).subscribe(res => {
      this.onReceiveBibtexFile(res);
      if (this.sub2) {
        this.sub2.unsubscribe();
      }
    });
  }

  onReceiveLatexFile(data: string): void {
    let json = this.classificationService.parseLatexToJs(data);
    FileUtilsService.saveAsJSONFile(json, "papers-classification");
  }

  onReceiveBibtexFile(data: string): void {
    let bibtex = this.papersService.cleanBibFile(data);
    FileUtilsService.saveAsTXTFile(bibtex, "papers.bib");
  }

}
