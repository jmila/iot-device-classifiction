import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PapersComponent } from './papers/papers.component';
import { PaperComponent } from './paper/paper.component';
import { ClassificationComponent } from './classification/classification.component';
import { ClassificationService } from './services/classification.service';
import { PapersService } from './services/papers.service';
import { RouterModule, Routes } from '@angular/router';
import { NavbarComponent } from './navbar/navbar.component';
import { MenuItemContent, MenuModule } from 'primeng/menu';
import { TabMenuModule } from 'primeng/tabmenu';
import { MenuItem } from 'primeng/api';
import { HttpClientModule } from '@angular/common/http';
import { PanelModule } from 'primeng/panel';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TableModule } from "primeng/table";
import { ButtonModule } from "primeng/button";
import { DialogModule } from 'primeng/dialog';
import { SelectButtonModule } from 'primeng/selectbutton';
import { FormsModule } from '@angular/forms';
import { BibFormatSelectorComponent } from './bib-format-selector/bib-format-selector.component';
import { ClassificationTable1Component } from './classification-table1/classification-table1.component';
import { ClassificationTable2Component } from './classification-table2/classification-table2.component';
import { ClassificationTable3Component } from './classification-table3/classification-table3.component';
import { ClassificationOverviewComponent } from './classification-overview/classification-overview.component';
import { OrganizationChartModule } from 'primeng/organizationchart';
import { InputSwitchModule } from 'primeng/inputswitch';
import { AdministrationComponent } from './administration/administration.component';
import { FileUtilsService } from './services/file-utils.service';
import { DetailedSankeyComponent } from './detailed-sankey/detailed-sankey.component';
import { ColorPickerModule } from 'primeng/colorpicker';
import { CustomDiagramComponent } from './custom-diagram/custom-diagram.component';
import { MultiSelectModule } from 'primeng/multiselect';
import { TabViewModule } from 'primeng/tabview';
import { ClassificationTableComponent } from './classification-table/classification-table.component';
import {ChartModule} from 'primeng/chart';
import { StatisticsComponent } from './statistics/statistics.component';

const appRoutes: Routes = [
  { path: "papers", component: PapersComponent },
  { path: "paper/:id", component: PaperComponent },
  { path: "classification", component: ClassificationComponent },
  { path: "admin", component: AdministrationComponent },
  { path: "classification/table", component: ClassificationTableComponent },
  { path: "classification/table1", component: ClassificationTable1Component },
  { path: "classification/table2", component: ClassificationTable2Component },
  { path: "classification/table3", component: ClassificationTable3Component },
  { path: "classification/sankey", component: DetailedSankeyComponent },
  { path: "classification/custom-diagram", component: CustomDiagramComponent },
  { path: "**", redirectTo: "classification" }
]

@NgModule({
  declarations: [
    AppComponent,
    PapersComponent,
    PaperComponent,
    ClassificationComponent,
    NavbarComponent,
    BibFormatSelectorComponent,
    ClassificationTable1Component,
    ClassificationTable2Component,
    ClassificationTable3Component,
    ClassificationOverviewComponent,
    AdministrationComponent,
    DetailedSankeyComponent,
    CustomDiagramComponent,
    ClassificationTableComponent,
    StatisticsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MenuModule,
    TabMenuModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    PanelModule,
    TableModule,
    ButtonModule,
    DialogModule,
    FormsModule,
    SelectButtonModule,
    OrganizationChartModule,
    InputSwitchModule,
    ColorPickerModule,
    MultiSelectModule,
    TabViewModule,
	ChartModule
  ],
  providers: [
    ClassificationService,
    PapersService,
    FileUtilsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
