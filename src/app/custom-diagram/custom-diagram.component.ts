import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import Two from 'two.js';
import { Text } from 'two.js/src/text';
import { PaperClassification } from '../models/paperclassification.model';
import { ClassificationService } from '../services/classification.service';
import { PapersService } from '../services/papers.service';

@Component({
  selector: 'app-custom-diagram',
  templateUrl: './custom-diagram.component.html',
  styleUrls: ['./custom-diagram.component.scss']
})
export class CustomDiagramComponent implements OnInit, OnDestroy {

  papersClassifications!: PaperClassification[];
  allLeavesLinksAmount !: any[]; // at visible leaf category index, get the amount of links

  twoInstance!: Two;
  notHoveredColor: string = "#c5c6c7";
  hoverText: Text | null = null;

  showNonHighlightedLinks = true;

  categories: any;

  categoriesSub !: Subscription;
  visibleLeafCategories: any[] = [];

  selectedPapersClassifications!: PaperClassification[];

  leafCategoryWidth !: number;
  leafInputX !: number;
  leafOutputX !: number;

  constructor(public classificationService: ClassificationService, private papersService: PapersService) { }

  ngOnInit(): void {
    this.classificationService.getPapersClassification()?.then(data => {
      this.papersClassifications = data;

      this.allLeavesLinksAmount = this.calculateLeavesLinksAmount();
      console.log("calculateLeavesLinksAmount: result: " + this.allLeavesLinksAmount);

      this.classificationService.emitCategories();
    });

    this.categoriesSub = this.classificationService.categoriesSubject.subscribe((categories) => {
      this.categories = categories;
      this.visibleLeafCategories = this.extractVisibleLeafCategories(this.categories);
      console.log("visibleLeafCategories: " + JSON.stringify(this.visibleLeafCategories));
      // this.visibleLeavesLinksAmount = this.getVisibleLeavesLinksAmount(this.allLeavesLinksAmount);
      // console.log("getVisibleLeavesLinksAmount: result: " + this.visibleLeavesLinksAmount);

      this.drawDiagram();
    });
  }

  ngOnDestroy(): void {
    this.categoriesSub.unsubscribe();
  }

  onSelectedPapersChange() {
    setTimeout(() => this.drawDiagram(), 10);
  }

  onButtonShowNonHoveredLinks(e: any) {
    if (e.checked) {
      this.notHoveredColor = "#c5c6c7";
    } else {
      this.notHoveredColor = "#ffffff";
    }
  }

  tempLeafIndex !: number;
  categoryColor = 'rgb(0, 200, 255)';
  categoryOpacity = 0.75;

  drawVisibleCategoriesIn(categoryNode: any, columnX: number, spaceBetweenCategoriesWidth: number, categoryWidth: number, categoryColor: string | null) {
    let categoryY = 0;
    if (categoryNode) {
      if (categoryNode.visible) {
        if (categoryColor == null) {
          categoryColor = categoryNode.color;
        }

        let childColumnX = columnX + (spaceBetweenCategoriesWidth + categoryWidth);
        if (categoryNode.label === this.classificationService.rootLabel) {
          childColumnX = 0.5 * categoryWidth;
          this.tempLeafIndex = 0;
        }

        let children = categoryNode.children;
        if (!children && categoryNode.label in this.leafCategoriesWithSubLeaves) {
          children = this.leafCategoriesWithSubLeaves[categoryNode.label];
        }

        let childrenCoords = [];
        if (children) {
          let visibleChildrenAmount = 0;
          for (let child of children) {
            let childCoords = this.drawVisibleCategoriesIn(child, childColumnX, spaceBetweenCategoriesWidth, categoryWidth, categoryColor);
            if (childCoords !== null) {
              childrenCoords.push(childCoords);
              categoryY += childCoords[1];
              visibleChildrenAmount++;
            }
          }
          categoryY /= visibleChildrenAmount;
        } else {
          let leafCat = this.visibleLeafCategories[this.tempLeafIndex];

          categoryY = leafCat.inputOutputY + ((leafCat.spaceBetweenLinks * (leafCat.linksAmount - 1)) * 0.5);
          this.tempLeafIndex++;

          return [this.leafOutputX, categoryY];
        }
        if (categoryNode.label !== this.classificationService.rootLabel) {
          let height = 30;
          var rect = this.twoInstance.makeRectangle(columnX, categoryY, categoryWidth, height);

          const catColor = categoryColor != null ? categoryColor : this.categoryColor;
          rect.fill = catColor;

          rect.linewidth = 0;
          rect.opacity = this.categoryOpacity;

          let catTitle = this.twoInstance.makeText(categoryNode.label, columnX, categoryY);
          catTitle.size = categoryNode.label.length > 45 ? 8 : categoryNode.label.length > 40 ? 9 : categoryNode.label.length > 35 ? 10 : categoryNode.label.length > 33 ? 11 : categoryNode.label.length > 30 ? 12 : 13;
          catTitle.fill = this.getTextColor(catColor);

          for (let childC of childrenCoords) {
            let linkLine = this.twoInstance.makeLine(columnX + (categoryWidth * 0.5), categoryY, childC[0], childC[1]);
          }
        }
        return [columnX - (categoryWidth * 0.5), categoryY];
      }
    }

    return null;
  }

  getTextColor(bgColor: string) {
    return "#000"; (parseInt(bgColor.replace('#', ''), 16) > 0xffffff / 2) ? '#000' : '#fff';
    var color = (bgColor.charAt(0) === '#') ? bgColor.substring(1, 7) : bgColor;
    var r = parseInt(color.substring(0, 2), 16); // hexToR
    var g = parseInt(color.substring(2, 4), 16); // hexToG
    var b = parseInt(color.substring(4, 6), 16); // hexToB
    return (((r * 0.299) + (g * 0.587) + (b * 0.114)) > 187) ?
      "#000000" : "#ffffff";
  }

  drawDiagram() {
    if (!this.visibleLeafCategories) {
      return;
    }
    if (this.twoInstance) {
      this.twoInstance.clear();
    } else {
      var params = {
        fitted: true
      };
      var elem = document.getElementById("custom-diagram");
      if (elem == null) {
        console.log("custom diagram not found");
        elem = document.body;
      }
      this.twoInstance = new Two(params).appendTo(elem);
    }

    var levelsAmount = 4;
    var columnsAmount = levelsAmount + 4; // + 1 for papers column

    var spaceWidthCoef = 0.4;
    //two.width = categoryWidth * columnsAmount + spaceWidthCoef * categoryWidth * columnsAmount;
    var categoryWidth = this.twoInstance.width / (columnsAmount * (1 + spaceWidthCoef));
    var spaceBetweenCategoriesWidth = categoryWidth * spaceWidthCoef;
    console.log("drawDiagram: categoryWidth: " + categoryWidth + ", spaceBetweenCategoriesWidth:" + spaceBetweenCategoriesWidth);

    // let line = this.twoInstance.makeLine(this.twoInstance.width, 0, this.twoInstance.width, this.twoInstance.height);
    // line.linewidth = 5;
    // line.stroke = "rgba(255, 0, 0, 0.5)";

    var topHeight = 10;
    var bottomHeight = 10;
    var spaceBetweenLeaves = 0.005 * this.twoInstance.height;
    var spaceBetweenParentCatLeaves = 0.015 * this.twoInstance.height; // bonus with spaceBetweenLeaves
    let categoryColor = 'rgb(0, 200, 255)';
    let categoryOpacity = 0.75;

    this.leafCategoryWidth = 2.5 * categoryWidth;

    let leavesColumnIndex = 3.75;
    let leafCategoriesX = (categoryWidth * 0.5) + leavesColumnIndex * (spaceBetweenCategoriesWidth + categoryWidth);
    let leavesAmount = this.visibleLeafCategories.length;
    let totalLinksAmount = 0;
    let lastVisibleParentCat = "";
    let visibleParentCatAmount = 0;
    for (let leafCategory of this.visibleLeafCategories) {
      totalLinksAmount += leafCategory.linksAmount;
      if (leafCategory.parentLabel != lastVisibleParentCat) {
        lastVisibleParentCat = leafCategory.parentLabel;
        visibleParentCatAmount++;
      }
    }

    let leavesSpace = this.twoInstance.height - topHeight - bottomHeight - (leavesAmount - 1) * spaceBetweenLeaves - (visibleParentCatAmount - 1) * spaceBetweenParentCatLeaves;
    let leafY = topHeight;

    this.leafInputX = leafCategoriesX + (this.leafCategoryWidth * 0.5);
    this.leafOutputX = leafCategoriesX - (this.leafCategoryWidth * 0.5);
    lastVisibleParentCat = "";
    for (let leafCategory of this.visibleLeafCategories) {
      if (lastVisibleParentCat.length == 0) {
        lastVisibleParentCat = leafCategory.parentLabel;
      } else if (leafCategory.parentLabel != lastVisibleParentCat) {
        lastVisibleParentCat = leafCategory.parentLabel;
        leafY += spaceBetweenParentCatLeaves;
      }

      let leafLinksAmount = leafCategory.linksAmount;

      let leafHeight = leavesSpace * (leafLinksAmount / totalLinksAmount);
      let leafInputOutputY = leafY;
      leafY += (leafHeight * 0.5);

      var rect = this.twoInstance.makeRectangle(leafCategoriesX, leafY, this.leafCategoryWidth, leafHeight);

      rect.fill = leafCategory.color;
      rect.linewidth = 0;
      rect.opacity = categoryOpacity;

      let leafTitle = this.twoInstance.makeText(leafCategory.label, leafCategoriesX, leafY);
      leafTitle.fill = this.getTextColor(leafCategory.color);

      let spaceBetweenLinks = leafLinksAmount > 0 ? leafHeight / leafLinksAmount : 0;
      leafInputOutputY += (spaceBetweenLinks * 0.5);

      // leafCategory.inputX = leafInputX;
      leafCategory.inputOutputY = leafInputOutputY;

      leafCategory.spaceBetweenLinks = spaceBetweenLinks;
      leafCategory.freeLinkIndex = 0;

      for (let inputIndex = 0; inputIndex < leafLinksAmount; inputIndex++) {
        this.twoInstance.makeCircle(this.leafInputX, leafInputOutputY, 1.5).fill = 'rgb(0,0,0)';
        // this.twoInstance.makeCircle(leafOutputX, leafInputOutputY, 1.5).fill = 'rgb(0,0,0)';
        leafInputOutputY += spaceBetweenLinks;
      }

      leafY += (leafHeight * 0.5) + spaceBetweenLeaves;
    }

    this.drawVisibleCategoriesIn(this.categories, 0, spaceBetweenCategoriesWidth, categoryWidth, null);

    // Links drawing
    if (this.selectedPapersClassifications) {
      let horizontalPadding = 25;
      let paperStartY = topHeight + horizontalPadding;
      let spaceBetweenPapers = (this.twoInstance.height - paperStartY - 2 * horizontalPadding) / (this.selectedPapersClassifications.length - 1);

      let paperColumnIndex = columnsAmount - 1;
      let paperStartX = (categoryWidth * 0.5) + paperColumnIndex * (spaceBetweenCategoriesWidth + categoryWidth);
      let totalPapersAmount = this.selectedPapersClassifications.length;

      let paperIndex = 0;
      let links: any = {};
      for (let paperClassif of this.selectedPapersClassifications) {
        if (!paperClassif.id) {
          continue;
        }
        if (paperClassif.data) {
          let paperColor = this.getRandColor(totalPapersAmount, paperIndex);
          for (let leaf of this.visibleLeafCategories) {
            let leafDataIndex = leaf.dataIndex;
            if ((paperClassif.data[leafDataIndex] === 1) || (paperClassif.data[leafDataIndex] !== 0 && typeof paperClassif.data[leafDataIndex] === 'string' && paperClassif.data[leafDataIndex].includes(leaf.label))) {
              let leafInputLinkIndex = leaf.freeLinkIndex;

              let leafInputY = leaf.inputOutputY + leafInputLinkIndex * leaf.spaceBetweenLinks;

              let linkLine = this.twoInstance.makeLine(paperStartX, paperStartY, this.leafInputX, leafInputY);
              linkLine.stroke = paperColor;

              if (!links[paperClassif.id]) {
                links[paperClassif.id] = [];
              }
              links[paperClassif.id].push(linkLine);

              leaf.freeLinkIndex++;
            }
          }
        } else {
          console.log("No data for paper class id: " + paperClassif.id);
        }

        paperStartY += spaceBetweenPapers;
        paperIndex++;
      }

      this.twoInstance.update();

      for (let paperId in links) {
        for (let link of links[paperId]) {
          let inst: CustomDiagramComponent = this;
          (<any>link._renderer).elem.addEventListener('mouseover', function () {
            // console.log("Hover on " + paperId);
            let firstLink = links[paperId][0];
            inst.hoverText = inst.twoInstance.makeText(inst.papersService.getPaperUniqueName(paperId), firstLink.vertices[0].x, firstLink.vertices[0].y + 10);
            for (let link of links[paperId]) {
              link.linewidth = 4;
            }
            for (let otherPaperId in links) {
              if (otherPaperId === paperId) {
                continue;
              }
              for (let link of links[otherPaperId]) {
                if (inst.notHoveredColor === "#ffffff") {
                  link.visible = false;
                } else {
                  link.previousStroke = link.stroke;
                  link.stroke = inst.notHoveredColor;
                }
              }
            }
            inst.twoInstance.update();
          }, false);

          (<any>link._renderer).elem.addEventListener('mouseout', function () {
            // console.log("Unhover on " + paperId);
            if (inst.hoverText !== null) {
              inst.hoverText.remove();
            }
            for (let link of links[paperId]) {
              link.linewidth = 1;
            }
            for (let otherPaperId in links) {
              if (otherPaperId === paperId) {
                continue;
              }
              for (let link of links[otherPaperId]) {
                link.visible = true;
                if (link.previousStroke) {
                  link.stroke = link.previousStroke;
                  delete link.previousStroke;
                }
              }
            }
            inst.twoInstance.update();
          }, false);
        }
      }
    } else {
      this.twoInstance.update();
    }
  }

  getRandomColor(): string {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

  /**
  * @param numOfSteps: Total number steps to get color, means total colors
  * @param step: The step number, means the order of the color
  */
  getRandColor(numOfSteps: number, step: number) {
    // This function generates vibrant, "evenly spaced" colours (i.e. no clustering). This is ideal for creating easily distinguishable vibrant markers in Google Maps and other apps.
    // Adam Cole, 2011-Sept-14
    // HSV to RBG adapted from: http://mjijackson.com/2008/02/rgb-to-hsl-and-rgb-to-hsv-color-model-conversion-algorithms-in-javascript
    var r = 0, g = 0, b = 0;
    var h = step / numOfSteps;
    var i = ~~(h * 6);
    var f = h * 6 - i;
    var q = 1 - f;
    switch (i % 6) {
      case 0: r = 1; g = f; b = 0; break;
      case 1: r = q; g = 1; b = 0; break;
      case 2: r = 0; g = 1; b = f; break;
      case 3: r = 0; g = q; b = 1; break;
      case 4: r = f; g = 0; b = 1; break;
      case 5: r = 1; g = 0; b = q; break;
    }
    var c = "#" + ("00" + (~ ~(r * 255)).toString(16)).slice(-2) + ("00" + (~ ~(g * 255)).toString(16)).slice(-2) + ("00" + (~ ~(b * 255)).toString(16)).slice(-2);
    return (c);
  }

  /**
   * 
   * @return links amount per leaves for all leaves, visible or not, ordered by dataIndex
   */
  calculateLeavesLinksAmount(): number[] {
    let linksAmount: any[] = [];
    for (let i = 0; i < 70; i++) { // 70 is higher than necessary
      linksAmount[i] = 0;
    }

    for (let paperClassif of this.papersClassifications) {
      if (paperClassif.data) {
        for (let dataIndex in paperClassif.data) {
          let dataValue = paperClassif.data[dataIndex];
          if (dataValue === 1) {
            linksAmount[dataIndex]++;
          } else if (dataValue !== 0 && typeof dataValue === 'string') {
            let values = dataValue.split(",");
            for (let value of values) {
              dataValue = value.trim();
              if (linksAmount[dataIndex] === 0) {
                linksAmount[dataIndex] = {};
              }
              if (!(dataValue in linksAmount[dataIndex])) {
                linksAmount[dataIndex][dataValue] = 0;
              }
              linksAmount[dataIndex][dataValue]++;
            }
          }
        }
      }
    }
    return linksAmount;
  }

  leafCategoriesWithSubLeaves: any = {};

  extractVisibleLeafCategories(categories: any): any[] {
    let tempCategories: any[] = [];
    this.leafCategoriesWithSubLeaves = {};

    let categoryNode = categories;
    if (categoryNode) {
      if (categoryNode.visible) {
        if (categoryNode.label === this.classificationService.rootLabel) {
          for (let topIndex = 0; topIndex < categoryNode.children.length; topIndex++) {
            let top = categoryNode.children[topIndex];
            let sectionColor = this.getRandColor(categoryNode.children.length, topIndex);
            top.color = sectionColor;
            let leaves: any[] = [];
            this.extractVisibleLeafCategoriesIn(top, leaves, top.label);
            for (let leaf of leaves) {
              let linksAmountObject = this.allLeavesLinksAmount[leaf.dataIndex];
              if (typeof linksAmountObject === 'number') {
                tempCategories.push({ label: leaf.label, dataIndex: leaf.dataIndex, linksAmount: <number>linksAmountObject, color: sectionColor, topLabel: top.label, parentLabel: leaf.parentLabel });
              } else {
                this.leafCategoriesWithSubLeaves[leaf.label] = [];
                for (let catStrValue in linksAmountObject) {
                  this.leafCategoriesWithSubLeaves[leaf.label].push({ label: catStrValue, visible: true });
                  tempCategories.push({ label: catStrValue, dataIndex: leaf.dataIndex, linksAmount: <number>linksAmountObject[catStrValue], color: sectionColor, topLabel: top.label, parentLabel: leaf.parentLabel });
                }
              }
            }
          }
        } else {
          console.log("extractVisibleLeafCategories: problem, root not found");
        }
      }
    }

    return tempCategories;
  }

  extractVisibleLeafCategoriesIn(categoryNode: any, result: any[], parentLabel: string) {
    if (categoryNode) {
      if (categoryNode.visible) {
        if (categoryNode.children) {
          for (let child of categoryNode.children) {
            this.extractVisibleLeafCategoriesIn(child, result, categoryNode.label);
          }
        } else {
          categoryNode.parentLabel = parentLabel;
          result.push(categoryNode);
        }
      }
    }
  }

}
