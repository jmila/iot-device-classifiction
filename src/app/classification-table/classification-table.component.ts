import { Component, OnDestroy, OnInit } from '@angular/core';
import { PaperClassification } from '../models/paperclassification.model';
import { ClassificationService } from '../services/classification.service';
import { DialogModule } from 'primeng/dialog';
import * as FileSaver from 'file-saver';
import { MenuItem } from 'primeng/api/menuitem';
import { HttpClient } from '@angular/common/http';
import { TreeNode } from 'primeng/api';
import { FileUtilsService } from '../services/file-utils.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-classification-table',
  templateUrl: './classification-table.component.html',
  styleUrls: ['./classification-table.component.scss']
})
export class ClassificationTableComponent implements OnInit, OnDestroy {

  papersClassifications!: PaperClassification[];

  displayBib: boolean = false;
  displayPaperOfId: string = "";

  categoriesSub !: Subscription;

  selectedBibFormat: string = 'SPLNCS03';

  visibleColumns: any[] = [];

  constructor(public classificationService: ClassificationService, private http: HttpClient) { }

  showDialog(paperId: string) {
    this.displayPaperOfId = paperId;
    this.displayBib = true;
  }

  ngOnInit() {
    this.classificationService.getPapersClassification()?.then(data => (this.papersClassifications = data));

    this.categoriesSub = this.classificationService.categoriesSubject.subscribe((categories) => {
      this.visibleColumns = this.extractVisibleColumns(categories);
      if (this.visibleColumns[0]) {
        this.visibleColumns[0].unshift({ label: "Paper", childrenAmount: 0 });
      }
    });
    this.classificationService.emitCategories();
  }

  ngOnDestroy(): void {
    this.categoriesSub.unsubscribe();
  }

  getLevelsAmount() {
    return this.visibleColumns.length;
  }

  tempColumns!: any[];

  /**
   * childrenAmount: colspan for the column
   * @param categories 
   * @returns 
   */
  extractVisibleColumns(categories: any): any[] {
    this.tempColumns = [];

    this.extractVisibleColumnsIn(categories, 0);
    // console.log("extractVisiColum: ");
    // this.tempColumns.forEach((v) => {
    //   v.forEach((c: { label: string; childrenAmount: number; }) => {
    //     console.log("extractVisiColum val:" + c.label + ": " + c.childrenAmount)
    //   })
    // }
    // );
    return this.tempColumns;
  }

  extractVisibleColumnsIn(categoryNode: any, level: number) {
    let visibleAmount = 0;
    if (categoryNode) {
      if (categoryNode.visible) {
        if (!this.tempColumns[level]) {
          this.tempColumns[level] = [];
        }

        let childLevel = level + 1;
        if (categoryNode.label === this.classificationService.rootLabel) {
          childLevel = 0;
        }
        if (categoryNode.children) {
          for (let child of categoryNode.children) {
            visibleAmount += this.extractVisibleColumnsIn(child, childLevel);
          }
        }
        if (categoryNode.label !== this.classificationService.rootLabel) {
          this.tempColumns[level].push({
            label: categoryNode.label,
            childrenAmount: visibleAmount
          });
          if (!categoryNode.children) {
            visibleAmount += 1;
          }
        }
      }
    }

    return visibleAmount;
  }

  onDownloadJSONButtonClick() {
    let visibleColumnsData: any[] = [];
    let visibleColumnsDataIndex = this.classificationService.getVisibleLeafCategoriesDataIndex();
    for (let paperClassif of this.papersClassifications) {
      let paperData: any[] = [];
      for (let i of visibleColumnsDataIndex) {
        let colData: any = paperClassif?.data ? paperClassif.data[i] : "";
        if (colData == undefined) {
          colData = "";
        }
        paperData.push(colData);
      }
      visibleColumnsData.push({ id: paperClassif.id, uniqueName: paperClassif.uniqueName, data: paperData });
    }

    FileUtilsService.saveAsJSONFile(visibleColumnsData, "classification");
  }

  onDownloadCSVButtonClick() {
    let visibleColumnsData: any[] = [];
    let visibleColumnsDataIndex = this.classificationService.getVisibleLeafCategoriesDataIndex();
    for (let paperClassif of this.papersClassifications) {
      let paperData: any[] = [paperClassif.id, paperClassif.uniqueName];
      for (let i of visibleColumnsDataIndex) {
        let colData: any = paperClassif?.data ? paperClassif.data[i] : "";
        if (colData == undefined) {
          colData = "";
        }
        paperData.push(colData);
      }
      visibleColumnsData.push(paperData);
    }

    FileUtilsService.saveAsCSVFile(visibleColumnsData, "classification");
  }

}
