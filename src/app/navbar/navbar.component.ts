import { Component, OnInit } from '@angular/core';
import { MenuModule } from 'primeng/menu';
import { MenuItem } from 'primeng/api';
import { TabMenuModule } from 'primeng/tabmenu';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  items: MenuItem[] = [];

  constructor() { }

  ngOnInit(): void {
    this.items = [
      { label: 'Classification', icon: 'pi pi-fw pi-home', routerLink: ['/classification'] },
      { label: 'Papiers', icon: 'pi pi-fw pi-file', routerLink: ['/papers'] },
      { label: 'Administration', icon: 'pi pi-fw pi-pencil', routerLink: ['/admin'] },
      // { label: 'Paramètres', icon: 'pi pi-fw pi-cog', routerLink: ['/settings'], disabled: true }
    ];
  }

}
