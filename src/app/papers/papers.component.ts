import { Component, OnDestroy, OnInit } from '@angular/core';
import { BibFilePresenter } from 'bibtex';
import { Subscription } from 'rxjs';
import { PapersService } from '../services/papers.service';

@Component({
  selector: 'app-papers',
  templateUrl: './papers.component.html',
  styleUrls: ['./papers.component.scss']
})
export class PapersComponent implements OnInit, OnDestroy {

  papersIds: string[] = [];
  private bibFileSub !: Subscription;

  constructor(public papersService: PapersService) { }

  ngOnInit(): void {
    this.bibFileSub = this.papersService.bibFileSubject.subscribe((bibFile: BibFilePresenter) => {
      let ids = this.papersService.getIds(bibFile);
      if (ids) {
        this.papersIds = ids;
      }
    });

    this.papersService.emitBibFile();

  }

  ngOnDestroy() {
    this.bibFileSub.unsubscribe();
  }

}
