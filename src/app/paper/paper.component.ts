import { Component, Inject, Input, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { BibEntry, BibFilePresenter, normalizeFieldValue, FieldValue } from "bibtex";
import { Paper } from '../models/paper.model';
import { PapersService } from '../services/papers.service';
import { PanelModule } from 'primeng/panel';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-paper',
  templateUrl: './paper.component.html',
  styleUrls: ['./paper.component.scss']
})
export class PaperComponent implements OnInit, OnDestroy {

  @Input() id!: string;
  @Input() closable: boolean = false;

  uniqueName: string = "Not found";

  bibDataFormat: string = 'SPLNCS03';
  bibDataFormatSub!: Subscription;
  bibFileSub!: Subscription;

  bibData!: BibEntry;
  paper: Paper | undefined;

  constructor(public papersService: PapersService) { }

  strIsDefined(str: string | undefined) {
    return str && str.length > 0 && str !== 'undefined';
  }

  ngOnInit(): void {
    this.uniqueName = "Not found";
    this.bibDataFormatSub = this.papersService.bibDataFormatSubject.subscribe((format: string) => {
      this.bibDataFormat = format;
    });
    this.papersService.emitBibDataFormat();

    this.bibFileSub = this.papersService.bibFileSubject.subscribe((bibFile: BibFilePresenter) => {
      this.updatePaper(bibFile);
    });
    this.papersService.emitBibDataFormat();
  }

  ngOnDestroy(): void {
    this.uniqueName = "Not found";
    this.bibDataFormatSub.unsubscribe();
    this.bibFileSub.unsubscribe();
  }

  formatAuthor(authors: string | undefined) {
    if (!authors || authors.length <= 0) {
      return "";
    }
    const authorsName = authors.split(' and ');
    var result = "";
    authorsName.forEach((authorName) => {
      if (result.length != 0) {
        result += ", ";
      }
      var authorNames = authorName.split(",");
      if (authorNames.length >= 2) {
        result += authorNames[0];
        var namePattern = /(?<=[A-Z])(.+)/g;
        result += ", " + authorNames[1].replace(namePattern, ".");
      } else {
        authorNames = authorName.split(" ");
        if (authorNames.length >= 2) {
          result += authorNames[1] + ", " + authorNames[0];
        } else {
          result += authorName;
        }
      }
    });
    return result;
  }

  formatPages(pages: string) {
    if (!pages || pages.length <= 0) {
      return "";
    }
    return pages.replace("--", "-");
  }

  updatePaper_() {
    if (this.papersService.bibFile) {
      this.updatePaper(this.papersService.bibFile);
    } else {
      this.papersService.emitBibFile();
    }
  }

  updatePaper(bibFile: BibFilePresenter) {
    let bib = bibFile.getEntry(this.id);
    if (bib) {
      this.bibData = bib;

      this.uniqueName = this.papersService.getPaperUniqueName(this.id);

      this.paper = {
        id: this.id,
        address: bib.getFieldAsString("address") + "",
        author: this.formatAuthor(bib.getFieldAsString("author") + ""),
        booktitle: bib.getFieldAsString("booktitle") + "",
        chapter: bib.getFieldAsString("chapter") + "",
        doi: bib.getFieldAsString("doi") + "",
        edition: bib.getFieldAsString("edition") + "",
        editor: bib.getFieldAsString("editor") + "",
        howpublished: bib.getFieldAsString("howpublished") + "",
        institution: bib.getFieldAsString("institution") + "",
        isbn: bib.getFieldAsString("isbn") + "",
        issn: bib.getFieldAsString("issn") + "",
        journal: bib.getFieldAsString("journal") + "",
        key: bib.getFieldAsString("key") + "",
        month: bib.getFieldAsString("month") + "",
        note: bib.getFieldAsString("note") + "",
        number: bib.getFieldAsString("number") + "",
        organization: bib.getFieldAsString("organization") + "",
        pages: this.formatPages(bib.getFieldAsString("pages") + ""),
        publisher: bib.getFieldAsString("publisher") + "",
        school: bib.getFieldAsString("school") + "",
        series: bib.getFieldAsString("series") + "",
        title: bib.getFieldAsString("title") + "",
        type: bib.type || "" + "",
        volume: bib.getFieldAsString("volume") + "",
        year: bib.getFieldAsString("year") + "",
        url: bib.getFieldAsString("url") + "",
        info: bib.getFieldAsString("info") + "",
        options: bib.getFieldAsString("options") + ""
      };
    } else {
      this.paper = undefined;
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['id'].currentValue !== changes['id'].previousValue) {
      this.uniqueName = "Not found";
      this.updatePaper_();
    }
  }

}
